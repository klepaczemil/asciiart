package pl.edu.pwr.pp;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class ImageFileWriter {

	public void saveToTxtFile (char[][] ascii, String fileName) throws IOException {
		// np. korzystając z java.io.PrintWriter
		PrintWriter writer = new PrintWriter(fileName, "UTF-8");	
		for(int i=0; i< ascii.length; i++){
			for(int j = 0; j< ascii[i].length; j++){
				writer.print(ascii[i][j]);
			}
			writer.println();
		}

		writer.close();
	}
	
}
