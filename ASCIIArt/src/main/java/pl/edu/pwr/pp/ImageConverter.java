package pl.edu.pwr.pp;

public class ImageConverter {

	/**
	 * Znaki odpowiadające kolejnym poziomom odcieni szarości - od czarnego (0)
	 * do białego (255).
	 */
	public static String INTENSITY_2_ASCII = "@%#*+=-:. ";

	/**
	 * Metoda zwraca znak odpowiadający danemu odcieniowi szarości. Odcienie
	 * szarości mogą przyjmować wartości z zakresu [0,255]. Zakres jest dzielony
	 * na równe przedziały, liczba przedziałów jest równa ilości znaków w
	 * {@value #INTENSITY_2_ASCII}. Zwracany znak jest znakiem dla przedziału,
	 * do którego należy zadany odcień szarości.
	 * 
	 * 
	 * @param intensity
	 *            odcień szarości w zakresie od 0 do 255
	 * @return znak odpowiadający zadanemu odcieniowi szarości
	 */
	public static char intensityToAscii(int intensity) {

		if (intensity < 26) {
			return '@';
		}
		if ((26 <= intensity) && (intensity < 52)) {
			return '%';
		}
		if ((52 <= intensity) && (intensity < 77)) {
			return '#';
		}
		if ((77 <= intensity) && (intensity < 103)) {
			return '*';
		}
		if ((103 <= intensity) && (intensity < 128)) {
			return '+';
		}
		if ((128 <= intensity) && (intensity < 154)) {
			return '=';
		}
		if ((154 <= intensity) && (intensity < 180)) {
			return '-';
		}
		if ((180 <= intensity) && (intensity < 205)) {
			return ':';
		}
		if ((205 <= intensity) && (intensity < 231)) {
			return '.';
		}
		if ((231 <= intensity) && (intensity <= 255)) {
			return ' ';
		} else
			// jesli z poza zakresu to pusty znak
			return ' ';
	}

	/**
	 * Metoda zwraca dwuwymiarową tablicę znaków ASCII mając dwuwymiarową
	 * tablicę odcieni szarości. Metoda iteruje po elementach tablicy odcieni
	 * szarości i dla każdego elementu wywołuje {@ref #intensityToAscii(int)}.
	 * 
	 * @param intensities
	 *            tablica odcieni szarości obrazu
	 * @return tablica znaków ASCII
	 */
	public static char[][] intensitiesToAscii(int[][] intensities) {
		int row = intensities.length;
		int col = intensities[0].length;
		char[][] tabascii = new char[row][col];
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < intensities[i].length; j++) {
				tabascii[i][j] = intensityToAscii(intensities[i][j]);
			}
		}
		return tabascii;
	}

}
